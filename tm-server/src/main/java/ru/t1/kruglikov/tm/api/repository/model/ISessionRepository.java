package ru.t1.kruglikov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.dto.model.SessionDTO;
import ru.t1.kruglikov.tm.enumerated.SessionSort;
import ru.t1.kruglikov.tm.model.Session;

import java.util.List;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

    void removeAll();

    void removeAll(@NotNull String userId);

    @Nullable
    List<Session> findAll(@Nullable SessionSort sort);

}

