package ru.t1.kruglikov.tm.log;


public enum OperationType {

    INSERT,
    UPDATE,
    DELETE

}