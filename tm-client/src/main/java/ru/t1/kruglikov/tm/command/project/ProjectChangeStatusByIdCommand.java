package ru.t1.kruglikov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.dto.request.project.ProjectChangeStatusByIdRequest;
import ru.t1.kruglikov.tm.enumerated.Status;
import ru.t1.kruglikov.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-change-status-by-id";

    @NotNull
    public static final String DESCRIPTION = "Change project status by id.";

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");

        System.out.println("Enter project id:");
        @Nullable final String projectId = TerminalUtil.nextLine();

        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);

        @Nullable final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(getToken());
        request.setProjectId(projectId);
        request.setStatus(status);
        projectEndpoint.changeStatusById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
