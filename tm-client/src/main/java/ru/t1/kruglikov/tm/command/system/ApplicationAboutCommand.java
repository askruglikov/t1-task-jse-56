package ru.t1.kruglikov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.dto.request.project.ProjectClearRequest;
import ru.t1.kruglikov.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.kruglikov.tm.dto.response.system.ApplicationAboutResponse;

@Component
public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String DESCRIPTION = "Show about developer.";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");

        @NotNull final ApplicationAboutResponse response = systemEndpoint.getAbout(new ApplicationAboutRequest());
        @NotNull final String name = response.getName();
        @NotNull final String email = response.getEmail();

        System.out.println("Name: " + name);
        System.out.println("Email: " + email);
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
