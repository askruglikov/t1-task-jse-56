package ru.t1.kruglikov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.dto.request.task.TaskClearRequest;

@Component
public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-clear";

    @NotNull
    public static final String DESCRIPTION = "Remove all tasks.";

    @Override
    public void execute() {
        System.out.println("[TASK CLEAR]");

        taskEndpoint.clear(new TaskClearRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
