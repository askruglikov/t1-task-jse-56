package ru.t1.kruglikov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.kruglikov.tm.util.TerminalUtil;

@Component
public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-create";

    @NotNull
    public static final String DESCRIPTION = "Create new project.";

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");

        System.out.println("Enter project name:");
        @Nullable final String name = TerminalUtil.nextLine();

        System.out.println("Enter project description:");
        @Nullable final String description = TerminalUtil.nextLine();

        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(getToken());
        request.setName(name);
        request.setDescription(description);
        projectEndpoint.create(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
