package ru.t1.kruglikov.tm.api.repository;

import ru.t1.kruglikov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> { }
