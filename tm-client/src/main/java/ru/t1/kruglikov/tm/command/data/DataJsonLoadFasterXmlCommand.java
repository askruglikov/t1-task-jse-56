package ru.t1.kruglikov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.dto.Domain;
import ru.t1.kruglikov.tm.dto.request.data.DataJsonLoadFasterXmlRequest;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public final class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-json";

    @NotNull
    public static final String DESCRIPTION = "Load data in json file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD JSON]");

        domainEndpoint.jsonLoadFasterXml(new DataJsonLoadFasterXmlRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
