package ru.t1.kruglikov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.dto.request.project.ProjectRemoveByIdRequest;
import ru.t1.kruglikov.tm.model.Project;
import ru.t1.kruglikov.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-remove-by-id";

    @NotNull
    public static final String DESCRIPTION = "Remove project by id.";

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");

        System.out.println("Enter project id:");
        @Nullable final String projectId = TerminalUtil.nextLine();

        @Nullable final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(getToken());
        request.setProjectId(projectId);
        projectEndpoint.removeById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
