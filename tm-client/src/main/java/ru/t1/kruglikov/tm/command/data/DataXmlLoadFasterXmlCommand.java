package ru.t1.kruglikov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.dto.Domain;
import ru.t1.kruglikov.tm.dto.request.data.DataXmlLoadFasterXmlRequest;

import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public final class DataXmlLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-xml";

    @NotNull
    public static final String DESCRIPTION = "Load data in json xml.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD XML]");

        domainEndpoint.xmlLoadFasterXml(new DataXmlLoadFasterXmlRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
