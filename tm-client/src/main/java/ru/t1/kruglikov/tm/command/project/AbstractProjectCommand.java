package ru.t1.kruglikov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.kruglikov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.kruglikov.tm.command.AbstractCommand;
import ru.t1.kruglikov.tm.dto.model.ProjectDTO;
import ru.t1.kruglikov.tm.enumerated.Role;
import ru.t1.kruglikov.tm.enumerated.Status;
import ru.t1.kruglikov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kruglikov.tm.model.Project;

@Component
public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected IProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showProject(final ProjectDTO project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + Status.toName(project.getStatus()));
    }

}
