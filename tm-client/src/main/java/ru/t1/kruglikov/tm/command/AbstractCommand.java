package ru.t1.kruglikov.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.api.command.ICommand;
import ru.t1.kruglikov.tm.api.service.ILocatorService;
import ru.t1.kruglikov.tm.enumerated.Role;

import java.util.Iterator;

@Getter
@Setter
@Component
public abstract class AbstractCommand implements ICommand {

    @Nullable
    @Autowired
    protected ILocatorService locatorService;

    public abstract void execute();

    @Nullable
    public abstract String getArgument();

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    @Nullable
    public abstract Role[] getRoles();

    @Nullable
    protected String getToken() {
        return getLocatorService().getTokenService().getToken();
    }

    protected void setToken(@Nullable final String token) {
        getLocatorService().getTokenService().setToken(token);
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();

        String displayName = "";
        if (name != null && !name.isEmpty()) displayName += name;
        if (argument != null && !argument.isEmpty()) displayName += ", " + argument;
        if (description != null && !description.isEmpty()) displayName += ": " + description;

        return displayName;
    }

}
