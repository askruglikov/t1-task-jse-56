package ru.t1.kruglikov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.dto.request.task.TaskStartByIndexRequest;
import ru.t1.kruglikov.tm.enumerated.Status;
import ru.t1.kruglikov.tm.util.TerminalUtil;

@Component
public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-start-by-index";

    @NotNull
    public static final String DESCRIPTION = "Start task by index.";

    @Override
    public void execute() {
        System.out.println("[START TASK BY INDEX]");

        System.out.println("Enter task index:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(getToken());
        request.setIndex(index);
        taskEndpoint.startByIndex(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
