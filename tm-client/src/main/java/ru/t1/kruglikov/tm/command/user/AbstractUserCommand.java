package ru.t1.kruglikov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.kruglikov.tm.api.endpoint.IUserEndpoint;
import ru.t1.kruglikov.tm.command.AbstractCommand;

@Component
public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    protected IAuthEndpoint authEndpoint;

    @Nullable @Override
    public String getArgument() {
        return null;
    }

}
