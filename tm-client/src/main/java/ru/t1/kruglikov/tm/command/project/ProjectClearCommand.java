package ru.t1.kruglikov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.dto.request.project.ProjectClearRequest;

@Component
public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-clear";

    @NotNull
    public static final String DESCRIPTION = "Remove all projects.";

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAR]");

        projectEndpoint.clear(new ProjectClearRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
