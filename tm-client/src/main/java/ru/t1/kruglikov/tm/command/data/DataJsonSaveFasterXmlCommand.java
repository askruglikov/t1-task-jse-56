package ru.t1.kruglikov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.dto.Domain;
import ru.t1.kruglikov.tm.dto.request.data.DataJsonLoadJaxBRequest;
import ru.t1.kruglikov.tm.dto.request.data.DataJsonSaveFasterXmlRequest;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

@Component
public final class DataJsonSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-json";

    @NotNull
    public static final String DESCRIPTION = "Save data in json file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE JSON]");

        domainEndpoint.jsonSaveFasterXml(new DataJsonSaveFasterXmlRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
