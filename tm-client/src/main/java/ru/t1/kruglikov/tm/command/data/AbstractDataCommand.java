package ru.t1.kruglikov.tm.command.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.kruglikov.tm.command.AbstractCommand;
import ru.t1.kruglikov.tm.enumerated.Role;

@NoArgsConstructor
@Component
public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected IDomainEndpoint domainEndpoint;

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
