package ru.t1.kruglikov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.dto.request.user.UserUpdateProfileRequest;
import ru.t1.kruglikov.tm.enumerated.Role;
import ru.t1.kruglikov.tm.util.TerminalUtil;

@Component
public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull private final String NAME = "user-update-profile";
    @NotNull private final String DESCRIPTION = "Update profile user.";

    @Override
    public void execute() {
        System.out.println("[USER UPDATE PROFILE]");

        System.out.println("Enter first name:");
        @Nullable final String firstName = TerminalUtil.nextLine();

        System.out.println("Enter last name:");
        @Nullable final String lastName = TerminalUtil.nextLine();

        System.out.println("Enter middle name:");
        @Nullable final String middleName = TerminalUtil.nextLine();

        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(getToken());
        request.setFirstName(firstName);
        request.setLastName(lastName);
        request.setMiddleName(middleName);
        userEndpoint.updateProfile(request);
    }

    @NotNull @Override
    public String getName() {
        return NAME;
    }

    @NotNull @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
