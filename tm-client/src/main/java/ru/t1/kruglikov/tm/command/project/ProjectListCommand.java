package ru.t1.kruglikov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.dto.model.ProjectDTO;
import ru.t1.kruglikov.tm.dto.request.project.ProjectListRequest;
import ru.t1.kruglikov.tm.enumerated.ProjectSort;
import ru.t1.kruglikov.tm.model.Project;
import ru.t1.kruglikov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

@Component
public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-list";

    @NotNull
    public static final String DESCRIPTION = "Show project list.";

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");

        System.out.println("Enter sort:");
        System.out.println(Arrays.toString(ProjectSort.values()));
        @Nullable final String sortType = TerminalUtil.nextLine();
        @Nullable final ProjectSort sort = ProjectSort.toSort(sortType);

        @NotNull final ProjectListRequest request = new ProjectListRequest(getToken());
        request.setSort(sort);
        @NotNull final List<ProjectDTO> projects = projectEndpoint.list(request).getProjects();

        int index = 1;
        for (@Nullable final ProjectDTO project : projects) {
            if (project == null) continue;
            System.out.println(index++ + ". " + project.getName() + ": " + project.getDescription());
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
