package ru.t1.kruglikov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.kruglikov.tm.dto.response.system.ApplicationVersionResponse;

@Component
public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-v";

    @NotNull
    public static final String NAME = "version";

    @NotNull
    public static final String DESCRIPTION = "Show version.";

    @Override
    public void execute() {
        System.out.println("[VERSION]");

        @NotNull final ApplicationVersionResponse response = systemEndpoint.getVersion(new ApplicationVersionRequest());
        @NotNull final String version = response.getVersion();

        System.out.println(version);
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
