package ru.t1.kruglikov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.dto.Domain;
import ru.t1.kruglikov.tm.dto.request.data.DataBase64SaveRequest;
import sun.misc.BASE64Encoder;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

@Component
public final class DataBase64SaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-base64";

    @NotNull
    public static final String DESCRIPTION = "Save data to base64 file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA BASE64 BINARY]");

        domainEndpoint.base64Save(new DataBase64SaveRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
