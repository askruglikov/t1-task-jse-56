package ru.t1.kruglikov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.kruglikov.tm.enumerated.Role;

@Component
public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull private final String NAME = "user-logout";
    @NotNull private final String DESCRIPTION = "Log out.";

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");

        @NotNull final UserLogoutRequest request = new UserLogoutRequest(getToken());
        authEndpoint.logout(request);
    }

    @NotNull @Override
    public String getName() {
        return NAME;
    }

    @NotNull @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
